package cat.dam.edgar.permisos;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private Button btn_camera, btn_save;
    private Resources res;
    private final int MY_PERMISSIONS_REQUEST_CAMERA=1;
    private final int MY_PERMISSIONS_REQUEST_STORAGE=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setVariables();
        setListeners();
    }

    private void setVariables()
    {
        btn_camera = (Button) findViewById(R.id.btn_camera);
        btn_save = (Button) findViewById(R.id.btn_storage);
        res = getResources();
    }

    private void setListeners()
    {
        btn_camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cameraPermission();
            }
        });

        btn_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                savePermission();
            }
        });
    }

    private void cameraPermission()
    {
        if(haveCamera()) createToast(res.getString(R.string.camera_accepted));
        else ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_CAMERA);

    }

    private boolean haveCamera()
    {
        return ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED;
    }

    private void savePermission()
    {
        if(haveStorage()) createToast(res.getString(R.string.storage_accepted));
        else ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_STORAGE);
    }

    private boolean haveStorage()
    {
        return ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }

    private void createToast(String description)
    {
        Toast.makeText(getApplicationContext(), description, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults)
    {
        switch (requestCode){
            case MY_PERMISSIONS_REQUEST_CAMERA:
                if(haveCamera()) createToast(res.getString(R.string.camera_accepted));
                else createToast(res.getString(R.string.camera_denied));
                break;
            case MY_PERMISSIONS_REQUEST_STORAGE:
                if(haveStorage()) createToast(res.getString(R.string.storage_accepted));
                else createToast(res.getString(R.string.storage_denied));
                break;
        }
    }
}